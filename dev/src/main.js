// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import vueVimeoPlayer from 'vue-vimeo-player'
import Vue from 'vue'
import App from './App'
import router from './router'
var VueTouch = require('vue-touch')

Vue.use(vueVimeoPlayer)
Vue.config.productionTip = false
Vue.use(VueTouch, {name: 'v-touch'})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  data: {
    cadavreContent: [
       {id: 0, img: require('@/assets/op2/rosto8.jpg'), ida: true,  video: '337626350', name: 'Giovanna Mercadante', place: 'Austrália' , work: 'Estudante'}
      ,{id: 1, img: require('@/assets/op2/rosto5.jpg'), ida: true,  video: '337626071', name: 'Caroline Flores',     place: 'Irlanda'   , work: 'Chef de Cozinha'}
      ,{id: 2, img: require('@/assets/op2/rosto1.jpg'), ida: false, video: '338084537', name: 'Edwars Rodriguez',    place: 'Venezuela' , work: 'Engenheiro Industrial e Professor universitário'}
      ,{id: 3, img: require('@/assets/op2/rosto2.jpg'), ida: false, video: '337626734', name: 'Theo Ritrosi',        place: 'França'    , work: 'Chef de Cozinha'}
      ,{id: 4, img: require('@/assets/op2/rosto6.jpg'), ida: true,  video: '337626547', name: 'Marcella Morais',     place: 'Alemanha'  , work: 'Estudante'}
      ,{id: 5, img: require('@/assets/op2/rosto4.jpg'), ida: true,  video: '337625809', name: 'Edson Capoano',       place: 'Portugal'  , work: 'Jornalista e Professor Universitário'}
      ,{id: 6, img: require('@/assets/op2/rosto7.jpg'), ida: false, video: '338085076', name: 'Tadasi Itakaki',      place: 'Japão'     , work: 'Aposentado'}
    ]
  },
  components: { App },
  template: '<App/>'
})
