import Vue from 'vue'
import Router from 'vue-router'
import Abertura from '@/components/Abertura'
import Sobre from '@/components/Sobre'
import Video from '@/components/Video'
import Transition from '@/components/Transition'
import Deslocamentos from '@/components/Deslocamentos'
import Pagina from '@/components/pagina'
  // emigrar
  import mainEmigrante from '@/components/emigrar/Main'
  import PerfilEmigrante from '@/components/emigrar/Perfil'
  import DestinoEmigrante from '@/components/emigrar/Destinos'
  import PaisesEmigrante from '@/components/emigrar/Paises'
  import FugaEmigrante from '@/components/emigrar/Fuga'
  import XenofobiaEmigrante from '@/components/emigrar/Xenofobia'
  //imigrar
  import mainImigrante from '@/components/imigrar/Main'
  import PerfilImigrante from '@/components/imigrar/Perfil'
  import DireitoImigrante from '@/components/imigrar/Direitos'
  import MidiaImigrante from '@/components/imigrar/Midia'
  import XenofobiaImigrante from '@/components/imigrar/Xenofobia'
  import PazImigrante from '@/components/imigrar/Missao'
import Erro from '@/components/error'

import Loading from '@/components/loading'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/:id?',
      name: 'Home',
      component: Abertura,
    },
    {
      path: '/sobre',
      name: 'Sobre',
      component: Sobre
    },
    {
      path: '/video/:vidId',
      name: 'Video',
      component: Video
    },
    {
      path: '/idas',
      name: 'Idas',
      component: Transition,
      props: true,
    },
    {
      path: '/vindas',
      name: 'Vindas',
      component: Transition,
      props: true
    },
    {
      path: '/deslocamentos',
      name: 'Deslocamentos',
      component: Deslocamentos,
    },
    {
      path: '/imigrar',
      component: Pagina,
      props: { imigrar: true },
      children: [
        {
          path: '',
          name: 'Imigrar',
          component: mainImigrante,
          props: { pessoa: 'alan', video: 337626925 }
        },
        {
          path: 'perfil',
          name: 'PerfilImigrante',
          component: PerfilImigrante,
          props: { pessoa: 'alan', video: 337626964 }
        },
        {
          path: 'imigracao',
          name: 'DireitoImigrante',
          component: DireitoImigrante,
          props: { pessoa: 'diana', video: 337627123 }
        },
          {
            path: 'lei',
            name: 'LeiDireitoImigrante',
            component: DireitoImigrante,
            props: { pessoa: 'diana', video: 349082066 }
          },
          {
            path: 'direito',
            name: 'QuaisDireitoImigrante',
            component: DireitoImigrante,
            props: { pessoa: 'diana', video: 349082778 }
          },
        {
          path: 'midia',
          name: 'MidiaImigrante',
          component: MidiaImigrante,
          props: { pessoa: 'denise', video: 337627015 }
        },
          {
            path: 'midia',
            name: 'PassadoMidia',
            component: MidiaImigrante,
            props: { pessoa: 'denise', video: 349081721 }
          },
          {
            path: 'midia',
            name: 'DiversidadeMidia',
            component: MidiaImigrante,
            props: { pessoa: 'denise', video: 349082579 }
          },
          {
            path: 'midia',
            name: 'ProblemaMidia',
            component: MidiaImigrante,
            props: { pessoa: 'denise', video: 349082366 }
          },
          {
            path: 'midia',
            name: 'EstadoMidia',
            component: MidiaImigrante,
            props: { pessoa: 'denise', video: 349081886 }
          },
        {
          path: 'xenofobia',
          name: 'XenofobiaImigrante',
          component: XenofobiaImigrante,
          props: { pessoa: 'maura', video: 337627247 }
        },
        {
          path: 'missao_paz',
          name: 'PazImigrante',
          component: PazImigrante,
          props: { pessoa: 'paolo', video: 337627282 }
        },
        {
          path: 'estrutura',
          name: 'EstruturaPaz',
          component: PazImigrante,
          props: { pessoa: 'paolo', video: 337627409 }
        },
        {
          path: 'autonomia',
          name: 'AutonomiaPaz',
          component: PazImigrante,
          props: { pessoa: 'paolo', pessoaB: 'wellington', video: 337627513 }
        },
        {
          path: 'paolo',
          name: 'padrePaz',
          component: PazImigrante,
          props: { pessoa: 'paolo', video: 337627671 }
        },
        {
          path: 'wellington',
          name: 'wellingtonPaz',
          component: PazImigrante,
          props: { pessoa: 'wellington', video: 337627757 }
        },
    ]},
    {
      path: '/emigrar',
      component: Pagina,
      props: { imigrar: false },
      children: [
        {
          path: '',
          name: 'Emigrar',
          component: mainEmigrante,
          props: { pessoa: 'diana', video: 337627147 }
        },
        {
          path: 'perfil',
          name: 'PerfilEmigrante',
          component: PerfilEmigrante,
          props: { pessoa: 'diana', video: 349082481 }
        },
        {
          path: 'destino',
          name: 'DestinoEmigrante',
          component: DestinoEmigrante,
          props: { pessoa: 'diana', video: 337627166 }
        },
        {
          path: 'paises',
          name: 'PaisesEmigrante',
          component: PaisesEmigrante,
          props: { pessoa: 'alan', video: 338750096 }
        },
        {
          path: 'fuga',
          name: 'FugaEmigrante',
          component: FugaEmigrante,
          props: { pessoa: 'denise', video: 338749508 }
        },
        {
          path: 'xenofobia',
          name: 'XenofobiaEmigrante',
          component: XenofobiaEmigrante,
          props: { pessoa: 'maura', video: 338750183 }
        }
      ]},
      {
        path: '*',
        name: 'Home',
        component: Abertura
      },
    ]
})

export default router
